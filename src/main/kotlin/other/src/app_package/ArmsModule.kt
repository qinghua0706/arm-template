package other.src.app_package

import other.ArmsPluginTemplateProviderImpl
import other.armsAnnotation

fun armsModule(isKt: Boolean, provider: ArmsPluginTemplateProviderImpl) = if (isKt) armsModuleKt(provider) else armsModuleJava(provider)

private fun armsModuleKt(provider: ArmsPluginTemplateProviderImpl): String {
    return """
        
package ${provider.moudlePackageName.value}
import dagger.Module
import dagger.Provides
import ${provider.contractPackageName.value}.${provider.pageName.value}Contract
import ${provider.modelPackageName.value}.${provider.pageName.value}Model

$armsAnnotation
@Module
 //构建${provider.pageName.value}Module时,将View的实现类传进来,这样就可以提供View的实现类给presenter
class ${provider.pageName.value}Module(private val view:${provider.pageName.value}Contract.View) {
    @Provides
    fun provide${provider.pageName.value}View():${provider.pageName.value}Contract.View{
        return this.view
    }
    @Provides
    fun provide${provider.pageName.value}Model(model:${provider.pageName.value}Model):${provider.pageName.value}Contract.Model{
        return model
    }
}   
"""
}


fun armsModuleJava(provider: ArmsPluginTemplateProviderImpl): String {
    return """
package ${provider.moudlePackageName.value};

import dagger.Provides;
import dagger.Module;
import ${provider.contractPackageName.value}.${provider.pageName.value}Contract;
import ${provider.modelPackageName.value}.${provider.pageName.value}Model;

$armsAnnotation
@Module
 //构建${provider.pageName.value}Module时,将View的实现类传进来,这样就可以提供View的实现类给presenter
public class ${provider.pageName.value}Module {
     private ${provider.pageName.value}Contract.View view;

    /**
     * 构建${provider.pageName.value}Module时,将View的实现类传进来,这样就可以提供View的实现类给presenter
     * @param view
     */
    public ${provider.pageName.value}Module(${provider.pageName.value}Contract.View view) {
        this.view = view;
    }

    @Provides
    ${provider.pageName.value}Contract.View provide${provider.pageName.value}View(){
        return this.view;
    }

    @Provides
    ${provider.pageName.value}Contract.Model provide${provider.pageName.value}Model(${provider.pageName.value}Model model){
        return model;
    }
}   
"""
}